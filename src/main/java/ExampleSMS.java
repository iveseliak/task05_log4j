import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Auth Token at twilio.com/console
    public static final String ACCOUNT_SID = "ACa071c3bc3f64b3fe4f20a46047820f93";
    public static final String AUTH_TOKEN = "f64e36702b12e0c3c4ff21a45127139c";

    public static void send(String s){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380957161191"), // to
                        new PhoneNumber("+14694052732"), // from
                        s).create();
    }

    }
